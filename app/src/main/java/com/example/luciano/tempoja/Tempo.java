package com.example.luciano.tempoja;

import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.luciano.tempoja.params.Api;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Tempo extends AppCompatActivity {
    private final int DIAS = 7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Carlho: ", "e merda");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tempo);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String country = intent.getStringExtra("country");
        int humidity = intent.getIntExtra("humidity_max", 0);

        int[] temperature_min = new int[DIAS];
        int[] temperature_max = new int[DIAS];
        int[] rain_probability = new int[DIAS];
        String[] date_br = new String[DIAS];
        String[] text_pt = new String[DIAS];
        int[] icon_day = new int[DIAS];

        for(int i = 0; i < DIAS; i++) {
            temperature_max[i] = intent.getIntExtra("temperatura_max" + i,0);
            temperature_min[i] = intent.getIntExtra("temperatura_min" + i, 0);
            rain_probability[i] = intent.getIntExtra("rain_probability" + i, 0);
            date_br[i] = intent.getStringExtra("date_br" + i);
            text_pt[i] = intent.getStringExtra("text_pt" + i);
            String icon = intent.getStringExtra("icon_day" + i);
            switch (icon){
                case "1":
                    icon_day[i] = R.drawable.img1;
                    break;
                case "1n":
                    icon_day[i] = R.drawable.img1n;
                    break;
                case "2":
                    icon_day[i] = R.drawable.img2;
                    break;
                case "2n":
                    icon_day[i] = R.drawable.img2n;
                    break;
                case "2rn":
                    icon_day[i] = R.drawable.img2rn;
                    break;
                case "3":
                    icon_day[i] = R.drawable.img3;
                    break;
                case "3n":
                    icon_day[i] = R.drawable.img3n;
                    break;
                case "3tm":
                    icon_day[i] = R.drawable.img3tm;
                    break;
                case "4":
                    icon_day[i] = R.drawable.img4;
                    break;
                case "4n":
                    icon_day[i] = R.drawable.img4n;
                    break;
                case "4r":
                    icon_day[i] = R.drawable.img4r;
                    break;
                case "4rn":
                    icon_day[i] = R.drawable.img4rn;
                    break;
                case "4t":
                    icon_day[i] = R.drawable.img4t;
                    break;
                case "4tn":
                    icon_day[i] = R.drawable.img4tn;
                    break;
                case "5":
                    icon_day[i] = R.drawable.img5;
                    break;
                case "5n":
                    icon_day[i] = R.drawable.img5n;
                    break;
                case "6":
                    icon_day[i] = R.drawable.img6;
                    break;
                case "6n":
                    icon_day[i] = R.drawable.img6n;
                    break;
                case "7":
                    icon_day[i] = R.drawable.img7;
                    break;
                case "7n":
                    icon_day[i] = R.drawable.img7n;
                    break;
                case "8":
                    icon_day[i] = R.drawable.img8;
                    break;
                case "8n":
                    icon_day[i] = R.drawable.img8n;
                    break;
                case "9":
                    icon_day[i] = R.drawable.img9;
                    break;
                case "9n":
                    icon_day[i] = R.drawable.img9n;
                    break;
                default:
                    icon_day[i] = R.drawable.logo;
            }
        }

        TextView txt_temperatura_max = findViewById(R.id.max);
        TextView txt_temperatura_max1 = findViewById(R.id.max1);
        TextView txt_temperatura_max2 = findViewById(R.id.max2);
        TextView txt_temperatura_max3 = findViewById(R.id.max3);
        TextView txt_temperatura_max4 = findViewById(R.id.max4);
        TextView txt_temperatura_max5 = findViewById(R.id.max5);
        TextView txt_temperatura_max6 = findViewById(R.id.max6);

        txt_temperatura_max.setText("MAX: " + temperature_max[0]);
        txt_temperatura_max1.setText(temperature_max[1]);
        txt_temperatura_max2.setText(temperature_max[2]);
        txt_temperatura_max3.setText(temperature_max[3]);
        txt_temperatura_max4.setText(temperature_max[4]);
        txt_temperatura_max5.setText(temperature_max[5]);
        txt_temperatura_max6.setText(temperature_max[6]);

        TextView txt_temperatura_min = findViewById(R.id.min);
        TextView txt_temperatura_min1 = findViewById(R.id.min1);
        TextView txt_temperatura_min2 = findViewById(R.id.min2);
        TextView txt_temperatura_min3 = findViewById(R.id.min3);
        TextView txt_temperatura_min4 = findViewById(R.id.min4);
        TextView txt_temperatura_min5 = findViewById(R.id.min5);
        TextView txt_temperatura_min6 = findViewById(R.id.min6);

        txt_temperatura_min.setText("MIN: " + temperature_min[0]);
        txt_temperatura_min1.setText(temperature_min[1]);
        txt_temperatura_min2.setText(temperature_min[2]);
        txt_temperatura_min3.setText(temperature_min[3]);
        txt_temperatura_min4.setText(temperature_min[4]);
        txt_temperatura_min5.setText(temperature_min[5]);
        txt_temperatura_min6.setText(temperature_min[6]);

        //TextView txt_rain_probability = findViewById(R.id.chuva);
        TextView txt_rain_probability1 = findViewById(R.id.chuva1);
        TextView txt_rain_probability2 = findViewById(R.id.chuva2);
        TextView txt_rain_probability3 = findViewById(R.id.chuva3);
        TextView txt_rain_probability4 = findViewById(R.id.chuva4);
        TextView txt_rain_probability5 = findViewById(R.id.chuva5);
        TextView txt_rain_probability6 = findViewById(R.id.chuva6);

        //txt_rain_probability.setText("Chuva: " + rain_probability[0] + "%");
        txt_rain_probability1.setText(rain_probability[1] + "%");
        txt_rain_probability2.setText(rain_probability[2] + "%");
        txt_rain_probability3.setText(rain_probability[3] + "%");
        txt_rain_probability4.setText(rain_probability[4] + "%");
        txt_rain_probability5.setText(rain_probability[5] + "%");
        txt_rain_probability6.setText(rain_probability[6] + "%");


        TextView txt_data_br = findViewById(R.id.data);
        TextView txt_data_br1 = findViewById(R.id.data1);
        TextView txt_data_br2 = findViewById(R.id.data2);
        TextView txt_data_br3 = findViewById(R.id.data3);
        TextView txt_data_br4 = findViewById(R.id.data4);
        TextView txt_data_br5 = findViewById(R.id.data5);
        TextView txt_data_br6 = findViewById(R.id.data6);

        txt_data_br.setText(date_br[0]);
        txt_data_br1.setText(date_br[1]);
        txt_data_br2.setText(date_br[2]);
        txt_data_br3.setText(date_br[3]);
        txt_data_br4.setText(date_br[4]);
        txt_data_br5.setText(date_br[5]);
        txt_data_br6.setText(date_br[6]);

        ImageView img_icon = findViewById(R.id.imagem);
        ImageView img_icon1 = findViewById(R.id.imagem1);
        ImageView img_icon2 = findViewById(R.id.imagem2);
        ImageView img_icon3 = findViewById(R.id.imagem3);
        ImageView img_icon4 = findViewById(R.id.imagem4);
        ImageView img_icon5 = findViewById(R.id.imagem5);
        ImageView img_icon6 = findViewById(R.id.imagem6);

        img_icon.setImageResource(icon_day[0]);
        img_icon1.setImageResource(icon_day[1]);
        img_icon2.setImageResource(icon_day[2]);
        img_icon3.setImageResource(icon_day[3]);
        img_icon4.setImageResource(icon_day[4]);
        img_icon5.setImageResource(icon_day[5]);
        img_icon6.setImageResource(icon_day[6]);

        TextView txt_humidity = findViewById(R.id.humidade);
        txt_humidity.setText("UR: " + humidity + "%");

    }

}
