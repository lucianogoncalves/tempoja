package com.example.luciano.tempoja;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.luciano.tempoja.params.Api;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class Clima {
    private JSONArray arr;
    private ArrayList<InformacoesClima> listaInformacoesClima = new ArrayList();

    public Clima(Context context, String id_cidade){

        String url = Api.URL;// +
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Response", response.toString());
                         ArrayList<InformacoesClima> lista = new ArrayList();

                        try {
                            long id = response.getLong("id");
                            String name = response.getString("name");
                            String state = response.getString("state");
                            String country = response.getString("country");
                            Cidade cidade = new Cidade(id, name, state, country);

                            JSONArray arr = response.getJSONArray("data");
                            for(int i = 0; i < arr.length(); i++){
                                JSONObject o = arr.getJSONObject(i);
                                JSONObject rain = o.getJSONObject("rain");
                                JSONObject thermal_sensation = o.getJSONObject("thermal_sensation");
                                JSONObject text_icon = o.getJSONObject("text_icon");
                                JSONObject text = text_icon.getJSONObject("text");
                                JSONObject phrase = text.getJSONObject("phrase");
                                JSONObject humidity = o.getJSONObject("humidity");
                                JSONObject temperature = o.getJSONObject("temperature");
                                JSONObject temperature_morning = temperature.getJSONObject("morning");
                                JSONObject temperature_afternoon = temperature.getJSONObject("afternoon");
                                JSONObject temperature_night = temperature.getJSONObject("night");

                                String date_br = o.getString("date_br");
                                int rain_probability = rain.getInt("probability");
                                int rain_precipitation = rain.getInt("precipitation");
                                int thermal_sensation_max = thermal_sensation.getInt("max");
                                int thermal_sensation_min = thermal_sensation.getInt("min");
                                int humidity_min = humidity.getInt("min");
                                int humidity_max = humidity.getInt("max");
                                int temperature_max = temperature.getInt("max");
                                int temperature_min = temperature.getInt("min");
                                int temperature_morning_max = temperature_morning.getInt("max");
                                int temperature_morning_min = temperature_morning.getInt("min");
                                int temperature_afternoon_max = temperature_afternoon.getInt("max");
                                int temperature_afternoon_min = temperature_afternoon.getInt("min");
                                int temperature_night_max = temperature_night.getInt("max");
                                int temperature_night_min = temperature_night.getInt("min");

                                String text_pt = text.getString("pt");

                                Log.i("text", text_pt);

                                lista.add(new InformacoesClima(cidade, date_br, rain_probability,
                                        rain_precipitation, thermal_sensation_min, thermal_sensation_max,
                                        temperature_max, temperature_min, temperature_morning_max,
                                        temperature_morning_min, temperature_afternoon_max, temperature_afternoon_min,
                                        temperature_night_max, temperature_night_min, humidity_max,
                                        humidity_min, text_pt));
                            }
                            Clima.this.setListaInformacoesClima(lista);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());
                    }
                });
        requestQueue.add(jsonObjectRequest);
    }

    public ArrayList<InformacoesClima> getListaInformacoesClima() {
        return listaInformacoesClima;
    }

    public void setListaInformacoesClima(ArrayList<InformacoesClima> listaInformacoesClima) {
        this.listaInformacoesClima = listaInformacoesClima;
    }
}