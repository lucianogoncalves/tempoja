package com.example.luciano.tempoja;

public interface Resposta<T, S> {
    void onResponse(T response, S context);
}
