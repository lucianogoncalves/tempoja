package com.example.luciano.tempoja.params;

public final class Api {
    public static final String TOKEN = "c77644addc9818c28e53ddde926e5ca4";

    //public static final String URL = "http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/";
    public static final String URL = "http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/";
    public static final String URL_GET_FORECAST_FINAL = "/days/15?token=" + TOKEN;
    //6741xc77644addc9818c28e53ddde926e5ca4";
    public static final String URL_FIND_CITY = "http://apiadvisor.climatempo.com.br/api/v1/locale/city?name=";
    public static final String URL_FIND_CITY_END = "&token=" + TOKEN;

}
