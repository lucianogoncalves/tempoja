package com.example.luciano.tempoja;

public class InformacoesClima {
    private Cidade cidade;
    private String date_br;
    private int rain_probability;
    private int rain_precipitation;
    private int thermal_sensation_min;
    private int thermal_sensation_max;
    private int teperature_max;
    private int teperature_min;
    private int teperature_morning_max;
    private int teperature_morning_mix;
    private int teperature_afternoon_max;
    private int teperature_afternoon_mix;
    private int teperature_night_max;
    private int teperature_night_mix;
    private int humidity_max;
    private int humidity_min;

    private String text_pt;
    
    public InformacoesClima(Cidade cidade, String date_br, int rain_probability, int rain_precipitation, int thermal_sensation_min, int thermal_sensation_max, int teperature_max, int teperature_min, int teperature_morning_max, int teperature_morning_mix, int teperature_afternoon_max, int teperature_afternoon_mix, int teperature_night_max, int teperature_night_mix, int humidity_min, int humidity_max, String text_pt) {
        this.cidade = cidade;
        this.date_br = date_br;
        this.rain_probability = rain_probability;
        this.rain_precipitation = rain_precipitation;
        this.thermal_sensation_min = thermal_sensation_min;
        this.thermal_sensation_max = thermal_sensation_max;
        this.teperature_max = teperature_max;
        this.teperature_min = teperature_min;
        this.teperature_morning_max = teperature_morning_max;
        this.teperature_morning_mix = teperature_morning_mix;
        this.teperature_afternoon_max = teperature_afternoon_max;
        this.teperature_afternoon_mix = teperature_afternoon_mix;
        this.teperature_night_max = teperature_night_max;
        this.teperature_night_mix = teperature_night_mix;
        this.humidity_min = humidity_min;
        this.humidity_max = humidity_max;
        this.text_pt = text_pt;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public String getDate_br() {
        return date_br;
    }

    public int getRain_probability() {
        return rain_probability;
    }

    public int getRain_precipitation() {
        return rain_precipitation;
    }

    public int getThermal_sensation_min() {
        return thermal_sensation_min;
    }

    public int getThermal_sensation_max() {
        return thermal_sensation_max;
    }

    public int getTeperature_max() {
        return teperature_max;
    }

    public int getTeperature_min() {
        return teperature_min;
    }

    public String getText_pt() {
        return text_pt;
    }
}
