package com.example.luciano.tempoja;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.luciano.tempoja.params.Api;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buscarCidade(View view){
        EditText et = findViewById(R.id.txtNomeCidade);
        String nome = et.getText().toString();

        String url = Api.URL_FIND_CITY + nome + Api.URL_FIND_CITY_END;
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("response: ", response.toString());
                        try {
                            JSONObject object = response.getJSONObject(0);

                            Gson gson = new Gson();


                            long id = object.getLong("id");
                            String name = object.getString("name");
                            String state = object.getString("state");
                            String country = object.getString("country");

                            Intent intent = new Intent(getApplicationContext(), nadaActivity.class);
                            intent.putExtra("id", id);
                            intent.putExtra("name", name);
                            intent.putExtra("state", state);
                            intent.putExtra("country", country);

                            startActivity(intent);
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());
                    }
                });
        requestQueue.add(jsonObjectRequest);
    }

}